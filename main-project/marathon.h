#ifndef MARATHON_H
#define MARATHON_H

#include "constants.h"

struct Time
{
    int Hour;
    int Minute;
    int Seconds;
};

struct person
{
    int number;
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct marathon
{
    person reader;
    Time start;
    Time finish;
    char title[MAX_STRING_SIZE];
};

#endif
