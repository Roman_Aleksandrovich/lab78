#include "filter.h"
#include <cstring>
#include <iostream>

marathon **filter_marathon(marathon* array[], int size, bool(check)(marathon element), int& result_size)
{
    marathon **result = new marathon * [size];
    result_size = 0;
    for (int i = 0; i < size; i++)
    {
        if (check( *array[i] ) )
        {
            result[result_size++] = array[i];
        }
    }
    return result;
}

bool check(marathon* element)
{
    return strcmp(element->title, "�������") == 0;
}

bool check_time(marathon* element)
{
    return element->start.Hour > 2 && element->start.Minute > 50 && element->start.Seconds > 00;
}