#include "file_reader.h"
#include "constants.h"
#include "marathon.h"

#include <fstream>
#include <cstring>
Time convert(char* str)
{
    Time result;
    char* context = NULL;
    char* str_number = strtok_s(str, ":", &context);
    result.Hour = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.Minute = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.Seconds = atoi(str_number);
    return result;
}


void read(const char* file_name, marathon* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            marathon* item = new marathon;
            file >> item->reader.number;
            file >> item->reader.last_name;
            file >> item->reader.first_name;
            file >> item->reader.middle_name;
            file >> tmp_buffer;
            item->start = convert(tmp_buffer);
            file >> tmp_buffer;
            item->finish = convert(tmp_buffer);

            file.getline(item->title, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}